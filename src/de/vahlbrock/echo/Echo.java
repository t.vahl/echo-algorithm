package de.vahlbrock.echo;

public class Echo<C> {
    private final Explorer broadcast;
    private final C result;

    public Echo(Explorer broadcast, C result) {
        this.broadcast = broadcast;
        this.result = result;
    }

    public Explorer broadcast() {
        return this.broadcast;
    }

    public C getResult() {
        return this.result;
    }

    @Override
    public String toString() {
        return result.toString();
    }
}
