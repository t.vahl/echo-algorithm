package de.vahlbrock.echo;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class LeakingNode extends Node {
    public final List<String> receivedMessages = new ArrayList<>();

    @Override
    protected void receive(Connection input, Broadcast<?> broadcast) {
        this.receivedMessages.add((String) broadcast.getMessage());
    }
}

class NodeTest {
    @Test
    public void forwardsMessageToChildNodesOnce() {
        Node source = new Node();
        LeakingNode receiver = new LeakingNode();
        String testMessage = "testMessage";
        Broadcast<String> broadcast = new Broadcast<>(testMessage);
        Connection.immediate(receiver, source);

        source.receive(null, broadcast);
        source.receive(null, broadcast);

        assertArrayEquals(new String[]{testMessage}, receiver.receivedMessages.toArray());
    }

    @Test
    public void sendsMessagesAsync() throws InterruptedException {
        Node source = new Node();
        LeakingNode receiver = new LeakingNode();
        String testMessage = "testMessage";
        Connection.delayed(source, receiver, 100);

        source.send(testMessage);

        assertArrayEquals(new String[]{}, receiver.receivedMessages.toArray());

        Thread.sleep(200);

        assertArrayEquals(new String[]{testMessage}, receiver.receivedMessages.toArray());
    }
}