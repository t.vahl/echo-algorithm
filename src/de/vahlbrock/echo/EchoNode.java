package de.vahlbrock.echo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class EchoNode<C> extends Node {
    protected final List<EchoConnection<C>> echoConnections = new ArrayList<>();
    protected final HashMap<Integer, ExplorerState<C>> broadcastInfos = new HashMap<>();

    public void add(EchoConnection<C> connection) {
        this.echoConnections.add(connection);
    }

    public void send(Callback<C> callback) {
        this.send(null, callback);
    }

    public void send(Object message, Callback<C> callback) {
        Explorer broadcast = new Explorer(message);

        ExplorerState<C> info = new ExplorerState<>(this, broadcast, callback);
        this.broadcastInfos.put(broadcast.getId(), info);
        info.setPendingConnections(this.echoConnections);

        this.forwardBroadcast(broadcast, null);
    }

    protected void receive(Connection from, Broadcast<?> broadcast) {
        this.handleBroadcast(from, broadcast);
    }

    protected void receive(EchoConnection<C> from, Echo<C> echo) {
        ExplorerState<C> info = this.broadcastInfos.get(echo.broadcast().getId());
        this.onEcho(echo);
        info.addEcho(echo, from);
        if (info.mayFinish()) {
            info.finish(this.createMerged(echo.broadcast()));
        }
    }

    protected void handleBroadcast(Connection from, Broadcast<?> broadcast) {
        if (broadcast instanceof Explorer && from instanceof EchoConnection) {
            this.onExplorer((Explorer) broadcast);
            this.handleExplorer((EchoConnection<C>) from, (Explorer) broadcast);
        } else {
            super.receive(from, broadcast);
        }
    }

    protected void handleExplorer(EchoConnection<C> origin, Explorer broadcast) {
        int broadcastId = broadcast.getId();

        if (knownBroadcasts.contains(broadcastId)) {
            ExplorerState<C> info = this.broadcastInfos.get(broadcastId);
            origin.transmit(this, new Echo<>(info.broadcast, this.createDuplicate(broadcast)));
        } else {
            ExplorerState<C> info = new ExplorerState<>(this, broadcast, origin);
            this.broadcastInfos.put(broadcastId, info);
            info.setPendingConnections(this.otherEchoConnectionsThan(origin));
            super.receive(origin, broadcast);
        }

        if (this.isSinkNode()) {
            ExplorerState<C> info = this.broadcastInfos.get(broadcastId);
            info.finish(this.createInitial(broadcast));
        }
    }

    protected boolean isSinkNode() {
        return this.connections.size() <= 1;
    }

    protected List<EchoConnection<C>> otherEchoConnectionsThan(EchoConnection<C> except) {
        return this.echoConnections.stream().filter((c) -> c != except).collect(Collectors.toList());
    }

    protected void onEcho(Echo<C> echo) {
    }

    protected void onExplorer(Explorer explorer) {
    }

    protected C createDuplicate(Explorer explorer) {
        return this.createInitial(explorer);
    }

    protected C createInitial(Explorer explorer) {
        return this.createMerged(explorer);
    }

    protected C createMerged(Explorer explorer) {
        return null;
    }

    public interface Callback<C> {
        void onDone(C result);
    }
}
