package de.vahlbrock.echo;

public class Connection {
    protected final Node node;
    protected final Node secondNode;
    protected final int delay;

    protected Connection(Node node, Node secondNode, int delay) {
        this.node = node;
        this.secondNode = secondNode;
        this.delay = delay;

        node.add(this);
        secondNode.add(this);
    }

    public static void immediate(Node node, Node secondNode) {
        Connection.delayed(node, secondNode, 0);
    }

    public static void delayed(Node node, Node secondNode, int delay) {
        new Connection(node, secondNode, delay);
    }

    public Node otherThan(Node node) {
        if (node == this.node) {
            return this.secondNode;
        } else if (node == this.secondNode) {
            return this.node;
        }
        return null;
    }

    public void transmit(Node origin, Broadcast<?> broadcast) {
        Node destination = this.otherThan(origin);
        System.out.println("Broadcast from " + origin.toString() + " to " + destination.toString());
        Runnable transmission = () -> destination.receive(this, broadcast);
        this.executeTransmission(transmission);
    }

    protected void executeTransmission(Runnable transmission) {
        if (delay > 0) {
            this.delay(transmission);
        } else {
            transmission.run();
        }
    }

    protected void delay(Runnable r) {
        Runnable delayRunnable = () -> {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException ignored) {
                Thread.currentThread().interrupt();
            }
            r.run();
        };
        new Thread(delayRunnable).start();
    }
}
