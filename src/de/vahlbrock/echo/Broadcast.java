package de.vahlbrock.echo;

public class Broadcast<C> {
    private static int counter = 0;
    private final int id = counter++;
    private final C message;

    public Broadcast(C message) {
        this.message = message;
    }

    public C getMessage() {
        return message;
    }

    public int getId() {
        return id;
    }

}
