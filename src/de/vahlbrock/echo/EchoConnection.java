package de.vahlbrock.echo;

public class EchoConnection<C> extends Connection {
    protected final EchoNode<C> node;
    protected final EchoNode<C> secondNode;

    public EchoConnection(EchoNode<C> node, EchoNode<C> secondNode, int delay) {
        super(node, secondNode, delay);
        this.node = node;
        this.secondNode = secondNode;
        node.add(this);
        secondNode.add(this);
    }

    public static <C> void immediate(EchoNode<C> node, EchoNode<C> secondNode) {
        EchoConnection.delayed(node, secondNode, 0);
    }

    public static <C> void delayed(EchoNode<C> node, EchoNode<C> secondNode, int delay) {
        new EchoConnection<>(node, secondNode, delay);
    }

    public EchoNode<C> otherThan(EchoNode<C> node) {
        if (node == this.node) {
            return this.secondNode;
        } else if (node == this.secondNode) {
            return this.node;
        }
        return null;
    }

    public void transmit(EchoNode<C> origin, Echo<C> echo) {
        EchoNode<C> destination = this.otherThan(origin);
        System.out.println("Echo (" + echo.toString() + ") from " + origin.toString() + " to " + destination.toString());
        Runnable transmission = () -> destination.receive(this, echo);
        this.executeTransmission(transmission);
    }
}
