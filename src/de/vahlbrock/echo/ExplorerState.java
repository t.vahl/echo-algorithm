package de.vahlbrock.echo;

import java.util.ArrayList;
import java.util.List;

public class ExplorerState<C> {
    protected final EchoNode<C> echoNode;
    protected final Explorer broadcast;
    protected final List<Echo<C>> receivedEchoes = new ArrayList<>();
    protected EchoNode.Callback<C> callback = null;
    protected EchoConnection<C> origin = null;
    protected List<EchoConnection<C>> pendingConnections = new ArrayList<>();

    protected boolean hasBeenPending = false;
    protected boolean done = false;

    private ExplorerState(EchoNode<C> echoNode, Explorer broadcast) {
        this.echoNode = echoNode;
        this.broadcast = broadcast;
    }

    public ExplorerState(
            EchoNode<C> echoNode,
            Explorer broadcast,
            EchoNode.Callback<C> callback
    ) {
        this(echoNode, broadcast);
        this.callback = callback;
    }

    public ExplorerState(
            EchoNode<C> echoNode,
            Explorer broadcast,
            EchoConnection<C> origin
    ) {
        this(echoNode, broadcast);
        this.origin = origin;
    }

    public boolean isPending() {
        return this.pendingConnections.size() > 0;
    }

    public void setPendingConnections(List<EchoConnection<C>> pendingConnections) {
        this.hasBeenPending = true;
        this.pendingConnections = pendingConnections;
    }

    public void addEcho(Echo<C> echo, EchoConnection<C> from) {
        this.receivedEchoes.add(echo);
        this.pendingConnections.remove(from);
    }

    public boolean mayFinish() {
        return this.hasBeenPending && !this.isPending() && !this.done;
    }

    public void finish(C result) {
        this.done = true;

        if (this.origin != null) {
            this.origin.transmit(echoNode, new Echo<>(broadcast, result));
        } else if (this.callback != null) {
            this.callback.onDone(result);
        }
    }
}
