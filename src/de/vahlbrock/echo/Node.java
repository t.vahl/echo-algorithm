package de.vahlbrock.echo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Node {
    protected final List<Connection> connections = new ArrayList<>();
    protected final List<Integer> knownBroadcasts = new ArrayList<>();

    public void add(Connection connection) {
        this.connections.add(connection);
    }

    public <C> void send(C message) {
        Broadcast<C> broadcast = new Broadcast<>(message);
        this.receive(null, broadcast);
    }

    protected void receive(Connection input, Broadcast<?> broadcast) {
        if (!this.knownBroadcasts.contains(broadcast.getId())) {
            this.knownBroadcasts.add(broadcast.getId());
            this.forwardBroadcast(broadcast, input);
        }
    }

    protected List<Connection> otherConnectionsThan(Connection except) {
        return this.connections.stream().filter(c -> !c.equals(except)).collect(Collectors.toList());
    }

    protected void forwardBroadcast(Broadcast<?> broadcast, Connection origin) {
        List<Connection> remaining = this.otherConnectionsThan(origin);
        this.transmitTo(remaining, broadcast);
    }

    protected void transmitTo(List<Connection> connections, Broadcast<?> broadcast) {
        connections.forEach(connection -> connection.transmit(this, broadcast));
    }
}
