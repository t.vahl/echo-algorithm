package de.vahlbrock.echo.examples;

import de.vahlbrock.echo.*;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Factory;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.model.MutableNode;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class SingleSourceSort {
    public static void main(String[] args) {

        MutableGraph g = Factory.mutGraph("RandomGraph");
        g.graphAttrs().add("nodesep", 0.5);
        List<EchoNode<Set<String>>> nodes = new ArrayList<>(List.of(
                namedNode("192.168.178.5"),
                namedNode("192.168.178.6"),
                namedNode("192.168.178.7"),
                namedNode("192.168.178.8"),
                namedNode("192.168.178.9"),
                namedNode("192.168.178.2"),
                namedNode("192.168.178.3"),
                namedNode("192.168.178.4")
        ));

        List<EchoNode<Set<String>>> connectedNodes = new ArrayList<>(List.of(namedNode("192.168.178.1")));
        Map<String, MutableNode> nodeMap = new HashMap<>();
        MutableNode mSource = Factory.mutNode("192.168.178.1");
        g.add(mSource);
        nodeMap.put("192.168.178.1", mSource);

        for (EchoNode<Set<String>> node : nodes) {
            List<EchoNode<Set<String>>> alreadyConnectedTo = new ArrayList<>();
            MutableNode mNode = Factory.mutNode(node.toString());

            do {
                EchoNode<Set<String>> connector = connectedNodes.get((int) Math.floor(Math.random() * connectedNodes.size()));
                if (!alreadyConnectedTo.contains(connector)) {
                    int delay = 75 + (int) (Math.random() * 50);
                    EchoConnection.delayed(node, connector, delay);

                    mNode.addLink(mNode.linkTo(nodeMap.get(connector.toString())).attrs().add("label", delay));

                    System.out.printf("Connecting %s to %s with delay of %d%n", node.toString(), connector.toString(), delay);
                    alreadyConnectedTo.add(connector);
                }
            } while (Math.random() > 0.3);

            g.add(mNode);
            nodeMap.put(node.toString(), mNode);

            connectedNodes.add(node);
        }

        try {
            Graphviz.fromGraph(g).width(1024).render(Format.PNG).toFile(new File("graphviz/graph.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        connectedNodes.get(0).send(o -> {
            for (String id : o) {
                System.out.println(id);
            }
        });
    }

    private static NamedSingleSourceSortNode namedNode(String name) {
        return new NamedSingleSourceSortNode(name);
    }

    private static class NamedSingleSourceSortNode extends NamedEchoNode<Set<String>> {
        protected Map<Integer, Set<String>> currentLists = new HashMap<>();

        public NamedSingleSourceSortNode(String name) {
            super(name);
        }

        @Override
        public Set<String> createMerged(Explorer explorer) {
            return this.currentListOf(explorer.getId());
        }

        private Set<String> currentListOf(int family) {
            Set<String> list = this.currentLists.get(family);
            if (list == null) {
                list = this.createInitial();
                this.currentLists.put(family, list);
            }
            return list;
        }

        @Override
        public Set<String> createInitial(Explorer explorer) {
            return this.createInitial();
        }

        protected Set<String> createInitial() {
            HashSet<String> set = new HashSet<>();
            set.add(this.name);
            return set;
        }

        @Override
        public void onEcho(Echo<Set<String>> echo) {
            this.currentListOf(echo.broadcast().getId()).addAll(echo.getResult());
        }
    }
}
