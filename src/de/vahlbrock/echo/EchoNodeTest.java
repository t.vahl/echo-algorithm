package de.vahlbrock.echo;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EchoNodeTest {
    @Test
    public void echoesWhenAllChildrenHaveEchoed() {
        EchoNode<Integer> source = new CountEchoNode("Source");
        EchoNode<Integer> forward = new CountEchoNode("Forward");
        EchoNode<Integer> sinkOne = new CountEchoNode("SinkOne");
        EchoNode<Integer> sinkTwo = new CountEchoNode("SinkTwo");
        EchoConnection.immediate(source, forward);
        EchoConnection.immediate(forward, sinkOne);
        EchoConnection.immediate(forward, sinkTwo);

        AtomicInteger callCounter = new AtomicInteger(0);
        source.send("count", (r) -> callCounter.incrementAndGet());

        assertEquals(1, callCounter.get());
    }

    @Test
    public void answersSecondBroadcastWithEcho() {
        EchoNode<Integer> source = new CountEchoNode("Source");
        EchoNode<Integer> testedNode = new CountEchoNode("TestedNode");
        BroadcastReflectionNode mirror = new BroadcastReflectionNode("Mirror");
        EchoConnection.immediate(source, testedNode);
        EchoConnection.immediate(testedNode, mirror);

        AtomicInteger callCounter = new AtomicInteger(0);
        source.send("count", (r) -> callCounter.incrementAndGet());

        assertEquals(1, callCounter.get());
        assertEquals(1, mirror.getReceivedEchoes().size());
    }

    @Test
    public void echoesWithResult() {
        EchoNode<Integer> source = new CountEchoNode("Source");
        EchoNode<Integer> forward = new CountEchoNode("Forward");
        EchoNode<Integer> destination = new CountEchoNode("Destination");
        EchoConnection.immediate(source, forward);
        EchoConnection.immediate(forward, destination);

        AtomicInteger result = new AtomicInteger(-1);
        EchoNode.Callback<Integer> callback = result::set;

        source.send(callback);

        assertEquals(3, result.get());
    }

    @Test
    public void doesNotUseResultsFromDuplicateBroadcasts() {
        EchoNode<Integer> source = new CountEchoNode("Source");
        EchoNode<Integer> testedNode = new CountEchoNode("TestedNode");
        EchoConnection.immediate(source, testedNode);
        EchoConnection.immediate(testedNode, new BroadcastReflectionNode("Mirror"));

        AtomicInteger result = new AtomicInteger(-1);
        EchoNode.Callback<Integer> callback = result::set;

        source.send(callback);

        assertEquals(2, result.get());
    }

    private static class CountEchoNode extends NamedEchoNode<Integer> {
        private int count = 1;
        public CountEchoNode(String name) {
            super(name);
        }

        @Override
        public Integer createMerged(Explorer explorer) {
            return this.count;
        }

        @Override
        public void onEcho(Echo<Integer> echo) {
            this.count += echo.getResult();
        }

        @Override
        public Integer createDuplicate(Explorer explorer) {
            return 0;
        }
    }


    private static class BroadcastReflectionNode extends CountEchoNode {
        private final List<Echo<Integer>> receivedEchoes = new ArrayList<>();

        public BroadcastReflectionNode(String name) {
            super(name);
        }

        @Override
        protected void receive(Connection from, Broadcast<?> broadcast) {
            this.broadcastInfos.put(broadcast.getId(), new ExplorerState<>(this, (Explorer) broadcast, (EchoConnection<Integer>) from));
            from.transmit(this, broadcast);
            knownBroadcasts.add(broadcast.getId());
            super.receive(from, broadcast);
        }

        @Override
        protected void receive(EchoConnection<Integer> from, Echo<Integer> echo) {
            super.receive(from, echo);
            this.receivedEchoes.add(echo);
        }

        public List<Echo<Integer>> getReceivedEchoes() {
            return receivedEchoes;
        }

        @Override
        public Integer createDuplicate(Explorer explorer) {
            return 0;
        }
    }
}
