package de.vahlbrock.echo.examples;

import de.vahlbrock.echo.*;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Factory;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.model.MutableNode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        MutableGraph g = Factory.mutGraph("RandomGraph");
        g.graphAttrs().add("nodesep", 0.5);
        List<EchoNode<Integer>> nodes = new ArrayList<>(List.of(
                new NamedCountEchoNode("One"),
                new NamedCountEchoNode("Two"),
                new NamedCountEchoNode("Three"),
                new NamedCountEchoNode("Four"),
                new NamedCountEchoNode("Five"),
                new NamedCountEchoNode("Six"),
                new NamedCountEchoNode("Seven"),
                new NamedCountEchoNode("Eight"),
                new NamedCountEchoNode("Nine"),
                new NamedCountEchoNode("Ten")
        ));

        List<EchoNode<Integer>> connectedNodes = new ArrayList<>(List.of(new NamedCountEchoNode("Source")));
        Map<String, MutableNode> nodeMap = new HashMap<>();
        MutableNode mSource = Factory.mutNode("Source");
        g.add(mSource);
        nodeMap.put("Source", mSource);

        for (EchoNode<Integer> node : nodes) {
            List<EchoNode<Integer>> alreadyConnectedTo = new ArrayList<>();
            MutableNode mNode = Factory.mutNode(node.toString());

            do {
                EchoNode<Integer> connector = connectedNodes.get((int) Math.floor(Math.random() * connectedNodes.size()));
                if (!alreadyConnectedTo.contains(connector)) {
                    int delay = 75 + (int) (Math.random() * 50);
                    EchoConnection.delayed(node, connector, delay);

                    mNode.addLink(mNode.linkTo(nodeMap.get(connector.toString())).attrs().add("label", delay));

                    System.out.printf("Connecting %s to %s with delay of %d%n", node.toString(), connector.toString(), delay);
                    alreadyConnectedTo.add(connector);
                }
            } while (Math.random() > 0.3);

            g.add(mNode);
            nodeMap.put(node.toString(), mNode);

            connectedNodes.add(node);
        }

        try {
            Graphviz.fromGraph(g).width(1024).render(Format.PNG).toFile(new File("graphviz/graph.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        connectedNodes.get(0).send(o -> System.out.printf("%d nodes%n", o));
    }

    private static class NamedCountEchoNode extends NamedEchoNode<Integer> {
        private int count = 1;
        public NamedCountEchoNode(String name) {
            super(name);
        }

        @Override
        public void onEcho(Echo<Integer> echo) {
            this.count += echo.getResult();
        }

        @Override
        public Integer createMerged(Explorer explorer) {
            return this.count;
        }

        @Override
        public Integer createDuplicate(Explorer explorer) {
            return 0;
        }
    }
}
