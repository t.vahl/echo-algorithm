package de.vahlbrock.echo;

public class NamedEchoNode<C> extends EchoNode<C> {
    protected final String name;

    public NamedEchoNode(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
