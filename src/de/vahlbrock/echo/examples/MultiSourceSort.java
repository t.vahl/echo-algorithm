package de.vahlbrock.echo.examples;

import de.vahlbrock.echo.*;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.Factory;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.model.MutableNode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultiSourceSort {
    public static void main(String[] args) {

        MutableGraph g = Factory.mutGraph("RandomGraph");
        g.graphAttrs().add("nodesep", 0.5);
        List<NamedMultiSourceSortNode> nodes = new ArrayList<>(List.of(
                new NamedMultiSourceSortNode("192.168.178.5").wake(),
                new NamedMultiSourceSortNode("192.168.178.6"),
                new NamedMultiSourceSortNode("192.168.178.7").wake(),
                new NamedMultiSourceSortNode("192.168.178.8"),
                new NamedMultiSourceSortNode("192.168.178.9"),
                new NamedMultiSourceSortNode("192.168.178.2").wake(),
                new NamedMultiSourceSortNode("192.168.178.3"),
                new NamedMultiSourceSortNode("192.168.178.4")
        ));

        List<EchoNode<Ranking>> connectedNodes = new ArrayList<>(List.of(
                new NamedMultiSourceSortNode("192.168.178.1")
        ));
        Map<String, MutableNode> nodeMap = new HashMap<>();
        MutableNode mSource = Factory.mutNode("192.168.178.1");
        g.add(mSource);
        nodeMap.put("192.168.178.1", mSource);

        for (EchoNode<Ranking> node : nodes) {
            List<EchoNode<Ranking>> alreadyConnectedTo = new ArrayList<>();
            MutableNode mNode = Factory.mutNode(node.toString());

            do {
                EchoNode<Ranking> connector = connectedNodes.get((int) Math.floor(Math.random() * connectedNodes.size()));
                if (!alreadyConnectedTo.contains(connector)) {
                    int delay = 75 + (int) (Math.random() * 50);
                    EchoConnection.delayed(node, connector, delay);

                    mNode.addLink(mNode.linkTo(nodeMap.get(connector.toString())).attrs().add("label", delay));

                    System.out.printf("Connecting %s to %s with delay of %d%n", node.toString(), connector.toString(), delay);
                    alreadyConnectedTo.add(connector);
                }
            } while (Math.random() > 0.3);

            g.add(mNode);
            nodeMap.put(node.toString(), mNode);

            connectedNodes.add(node);
        }

        try {
            Graphviz.fromGraph(g).width(1024).render(Format.PNG).toFile(new File("graphviz/graph.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class NamedMultiSourceSortNode extends NamedEchoNode<Ranking> {
        public enum State {
            ASLEEP,
            AWAKE,
            SHUTOFF
        }

        protected State currentState = State.ASLEEP;
        protected Map<String, Ranking> rankings = new HashMap<>();

        public NamedMultiSourceSortNode(String name) {
            super(name);
        }

        public NamedMultiSourceSortNode wake() {
            if (currentState != State.SHUTOFF) {
                currentState = State.AWAKE;
            }

            new Thread(() -> {
                try {
                    Thread.sleep(500);
                    this.enqueue();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
            return this;
        }

        public void reset() {
            this.currentState = State.ASLEEP;
        }

        protected void enqueue() {
            this.send(this, (r) -> System.out.println(
                    "Node "
                            + this.toString()
                            + " queued after "
                            + r.larger
                            + " but before "
                            + r.smaller
            ));
        }

        @Override
        protected void onExplorer(Explorer explorer) {
            if (this.currentState == State.ASLEEP) {
                this.currentState = State.SHUTOFF;
            }
        }

        @Override
        protected void onEcho(Echo<Ranking> echo) {
            NamedMultiSourceSortNode initiator = (NamedMultiSourceSortNode) echo.broadcast().getMessage();
            Ranking currentRanking = this.rankingOf(initiator);
            Ranking arrivingRanking = echo.getResult();

            if (this.isSmallerThan(arrivingRanking.larger, currentRanking.larger)) {
                currentRanking.larger = arrivingRanking.larger;
            }
            if (this.isLargerThan(arrivingRanking.smaller, currentRanking.smaller)) {
                currentRanking.smaller = arrivingRanking.smaller;
            }
        }

        protected Ranking rankingOf(NamedMultiSourceSortNode node) {
            Ranking ranking = this.rankings.get(node.name);
            if (ranking == null) {
                ranking = new Ranking();
                this.rankings.put(node.name, ranking);
            }
            return ranking;
        }

        @Override
        protected Ranking createMerged(Explorer explorer) {
            NamedMultiSourceSortNode initiator = (NamedMultiSourceSortNode) explorer.getMessage();
            Ranking currentRanking = this.rankingOf(initiator);

            if (initiator == this) {
                return currentRanking;
            }

            if (this.currentState.equals(State.AWAKE)) {
                if (this.isSmallerThan(name, currentRanking.larger) && !currentRanking.larger.equals(Ranking.TOP)) {
                    currentRanking.larger = name;
                }
                if (this.isLargerThan(name, currentRanking.smaller) && !currentRanking.smaller.equals(Ranking.BOT)) {
                    currentRanking.smaller = name;
                }
            }

            return currentRanking.copy();
        }

        @Override
        protected Ranking createInitial(Explorer explorer) {
            return this.createMerged(explorer);
        }

        @Override
        protected Ranking createDuplicate(Explorer explorer) {
            if (this.currentState == State.SHUTOFF) {
                return new Ranking();
            } else {
                NamedMultiSourceSortNode initiator = (NamedMultiSourceSortNode) explorer.getMessage();

                Ranking ranking = new Ranking();
                if (this.isLargerThan(initiator)) {
                    ranking.larger = this.name;
                } else if (this.isSmallerThan(initiator)) {
                    ranking.smaller = this.name;
                }

                return ranking;
            }
        }

        protected boolean isLargerThan(NamedMultiSourceSortNode node) {
            return this.isLargerThan(this.name, node.name);
        }

        protected boolean isLargerThan(String first, String second) {
            if (second.equals(Ranking.TOP) || first.equals(Ranking.BOT)) {
                return false;
            } else if (first.equals(Ranking.TOP) || second.equals(Ranking.BOT)) {
                return true;
            } else {
                return first.compareToIgnoreCase(second) < 0;
            }
        }

        protected boolean isSmallerThan(NamedMultiSourceSortNode node) {
            return this.isSmallerThan(this.name, node.name);
        }

        protected boolean isSmallerThan(String first, String second) {
            if (second.equals(Ranking.BOT) || first.equals(Ranking.TOP)) {
                return false;
            } else if (first.equals(Ranking.BOT) || second.equals(Ranking.TOP)) {
                return true;
            } else {
                return first.compareToIgnoreCase(second) > 0;
            }
        }
    }

    private static class Ranking {
        public static String TOP = "TOP";
        public static String BOT = "BOT";

        public String larger = TOP;
        public String smaller = BOT;

        public Ranking copy() {
            Ranking clone = new Ranking();
            clone.larger = larger;
            clone.smaller = smaller;
            return clone;
        }
    }
}
